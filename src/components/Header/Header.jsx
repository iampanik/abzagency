import React from 'react';
import { block } from 'bem-cn';

import { Container, MainMenu, UserPanel } from '../../modules';
import { Logo } from '../../elements';

import './Header.scss';

const b = block('header');

const Header = () => {
  return (
    <header className={b()}>
      <Container>
        <div className={b('inner')}>
          <Logo />
          <MainMenu />
          <UserPanel />
        </div>
      </Container>
    </header>
  )
};

export default Header;
