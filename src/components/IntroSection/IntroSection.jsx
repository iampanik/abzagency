import React from 'react';
import { block } from 'bem-cn';

import { Container } from '../../modules';
import { DefaultButton } from '../../elements';

import './IntroSection.scss';

const b = block('intro-section');

const IntroSection = () => {

  const title = 'Test assignment for Frontend Developer position';
  const text = 'We kindly remind you that your test assignment should <br/> be submitted as a link to github/bitbucket repository. <br/> Please be patient, we consider and respond to every application <br/> that meets minimum requirements. We look forward to your <br/> submission. Good luck!';

  return (
    <div className={b()}>
      <Container>
        <div className={b('inner')}>
          <div className={b('text')}>
            <h1>{title}</h1>
            <p>{text}</p>
            <DefaultButton>
              Sign in
            </DefaultButton>
          </div>
        </div>
      </Container>
    </div>
  )
};

export default IntroSection;
