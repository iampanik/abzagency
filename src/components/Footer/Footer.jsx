import React from 'react';
import { block } from 'bem-cn';

import { Container } from '../../modules';

const b = block('footer');

const Footer = () => {
  return (
    <footer className={b()}>
      <Container>
        <h2>Footer</h2>
        <h3>Footer h3</h3>
      </Container>
    </footer>
  )
};

export default Footer;
