import React from 'react';

import { Header, Footer, IntroSection } from '../../components';

function App() {
  return (
    <React.Fragment>
      <Header />
      <IntroSection />
      <Footer />
    </React.Fragment>
  );
}

export default App;
