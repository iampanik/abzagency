import React from 'react';
import { block } from 'bem-cn';

import './Container.scss';

const b = block('container');

const Container = ({ children }) => {
  return (
    <div className={b()}>
      {children}
    </div>
  )
};

export default Container;
