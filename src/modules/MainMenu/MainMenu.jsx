import React, { Component } from 'react';
import { block } from 'bem-cn';

import './MainMenu.scss';

const b = block('main-menu');

class MainMenu extends Component {

  menuLinks = [
    {title: 'About me', asset: 'aboutMe', id: 1},
    {title: 'Relationships', asset: 'relationships', id: 2},
    {title: 'Requirements', asset: 'requirements', id: 3},
    {title: 'Users', asset: 'users', id: 4},
    {title: 'Sign Up ', asset: 'signUp', id: 5},
  ];

  render() {
    return (
      <div className={b()}>
        {this.menuLinks.map(el => (
          <div className={b('item')} key={el.id}>
            <a href="/" className={b('link').mix('styled-link')}>{el.title}</a>
          </div>
        ))}
      </div>
    );
  }
}

export default MainMenu;
