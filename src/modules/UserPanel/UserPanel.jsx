import React from 'react';
import { block } from 'bem-cn';

import { Avatar, LogoutButton } from '../../elements';

import './UserPanel.scss';

const b = block('user-panel');

const UserPanel = () => {
  return (
    <div className={b()}>
      <div className={b('info')}>
        <span className={b('username')}>Superstar</span>
        <span className={b('email')}>Superstar@gmail.com</span>
      </div>
      <div className={b('col', { second: true })}>
        <Avatar />
      </div>
      <div className={b('col', { last: true })}>
        <LogoutButton />
      </div>
    </div>
  )
};

export default UserPanel;
