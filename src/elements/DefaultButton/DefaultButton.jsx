import React from 'react';
import { block } from 'bem-cn';

import './DefaultButton.scss';

const b = block('default-button');

const DefaultButton = ({ children }) => {
  return (
    <button className={b()}>
      <span className={b('text')}>
        {children}
      </span>
    </button>
  )
};

export default DefaultButton;
