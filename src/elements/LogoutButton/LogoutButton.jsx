import React from 'react';
import { block } from 'bem-cn';

import { ReactComponent as LogoutIcon } from './images/sign-out.svg';

import './LogoutButton.scss';

const b = block ('logout-button');

const LogoutButton = () => {
  return (
    <button className={b()}>
      <LogoutIcon className={b('svg')} />
    </button>
  )
};

export default LogoutButton;
