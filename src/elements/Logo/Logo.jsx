import React from 'react';
import { block } from 'bem-cn';

import { ReactComponent as LogoImage } from './images/logo.svg';

import './Logo.scss';

const b = block('logo');

const Logo = () => {
  return (
    <a href="/" className={b()}>
      <LogoImage className={b('svg')} />
    </a>
  )
};

export default Logo;
