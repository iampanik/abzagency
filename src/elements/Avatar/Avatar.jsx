import React from 'react';
import { block } from 'bem-cn';

import './Avatar.scss';

const b = block('avatar');

const Avatar = () => {
  return (
    <div className={b()}>
      <img src={process.env.PUBLIC_URL + '/assets/images/user-superstar-2x.jpg'} alt="Superstar" className={b('image')}/>
    </div>
  )
};

export default Avatar;
